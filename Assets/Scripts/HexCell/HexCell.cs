﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class HexCell : MonoBehaviour
{
    public HexCoordinates coordinates;
    public Vector3 height;

    public HexColider[] coliders;
    public Mesh hexMesh;
    public List<Vector3> vertices;
    public List<int> triangles;

    public HexCell leftCell;
    public HexCell rightCell;

    void Awake()
    {
        coliders = gameObject.GetComponentsInChildren<HexColider>();
        GetComponent<MeshFilter>().mesh = hexMesh = new Mesh();
        hexMesh.name = "HexMesh";
        vertices = new List<Vector3>();
        triangles = new List<int>();
    }

   

    public virtual void Triangulate()
    {
    }

    public void AddTriangle(Vector3 v1, Vector3 v2, Vector3 v3)
    {
        int vertexIndex = vertices.Count;
        vertices.Add(v1);
        vertices.Add(v2);
        vertices.Add(v3);
        triangles.Add(vertexIndex);
        triangles.Add(vertexIndex + 1);
        triangles.Add(vertexIndex + 2);
    }
}