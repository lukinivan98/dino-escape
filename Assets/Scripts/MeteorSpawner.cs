﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorSpawner : MonoBehaviour
{
    private HexGrid fieldInstance; 
    public Meteor meteorPrefab;

    float timeSinceLastSpawn;

    public float timeBetweenSpawns = 5f;
    public float spawnDistance = 50f;
    public Vector3 spawnHeight = new Vector3(0, 1000f, 0);


    public void Initialize(HexGrid field)
    {
        fieldInstance = field;
    }

    void FixedUpdate()
    {
        timeSinceLastSpawn += Time.deltaTime;
        if (timeSinceLastSpawn >= timeBetweenSpawns)
        {
            timeSinceLastSpawn -= timeBetweenSpawns;
            SpawnMeteor(fieldInstance.cells[Random.Range(fieldInstance.width*fieldInstance.height/2,fieldInstance.cells.Count)]);
        }
    }

    void SpawnMeteor(HexCell cell)
    {
        Meteor spawn = Instantiate<Meteor>(meteorPrefab);
        spawn.gameObject.transform.SetParent(transform);
        spawn.currentCell = cell;
        spawn.transform.localPosition = (cell.transform.localPosition + Random.onUnitSphere * spawnDistance + spawnHeight);
        spawn.Fall();
    }
}
