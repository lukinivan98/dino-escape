﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class SpikeCell : HexCell
{
    public override void Triangulate()
    {
        float step = Random.Range(0, gameObject.GetComponentInParent<HexGrid>().MaxStep);
        height = new Vector3(0, gameObject.GetComponentInParent<HexGrid>().h + step, 0);
        Vector3 center = gameObject.transform.localPosition;
        for (int i = 0; i < 6; i++)
        {
            AddTriangle(center, center + HexMetrics.corners[i + 1], center + HexMetrics.corners[i]);
        }
        for (int i = 0; i < 6; i++)
        {
            AddTriangle(center + height - new Vector3(0, 5f, 0), center + HexMetrics.corners[i] + height, center + HexMetrics.corners[i + 1] + height);
        }
        for (int i = 0; i < 6; i++)
        {
            AddTriangle(center + HexMetrics.corners[i], center + HexMetrics.corners[i + 1] + height, center + HexMetrics.corners[i] + height);
        }

        for (int i = 0; i < 6; i++)
        {
            AddTriangle(center + HexMetrics.corners[i + 1] + height, center + HexMetrics.corners[i], center + HexMetrics.corners[i + 1]);
        }
        for (int i = 0; i < coliders.Length; i++)
        {
            coliders[i].Initialise(gameObject.transform.localPosition + height / 2f, new Vector3(HexMetrics.innerRadius * 2, height.y, HexMetrics.outerRadius));
        }
        hexMesh.vertices = this.vertices.ToArray();
        hexMesh.triangles = this.triangles.ToArray();
        hexMesh.RecalculateNormals();
    }
}