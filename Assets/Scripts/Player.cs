﻿using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    HexGrid field;
    private HexCell currentCell;
    public float stepTime = 0.15f;
    public float oneStepTime = 0.01f;

    bool moving;

    public void Initialize(HexCell cell)
    {
        field = cell.gameObject.GetComponentInParent<HexGrid>();
        SetLocation(cell);
    }

    public void SetLocation(HexCell cell)
    {
        transform.localPosition = cell.transform.position + new Vector3(0,cell.height.y + 5f,0);
        currentCell = cell;
    }

    private void FixedUpdate()
    {
        if (!moving)
        {
            if ((Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)))
            {

                moving = true;
                StartCoroutine(MoveToCell(currentCell.leftCell));
                field.SpawnNewLine(0);
            }
            else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                moving = true;
                StartCoroutine(MoveToCell(currentCell.rightCell));
                field.SpawnNewLine(1);
            }
        }
        Component a = currentCell.GetComponentInParent<SpikeCell>();

        if (a != null)
        {
            GameManager.playerAlive = false;
        }
    }

    private IEnumerator MoveToCell(HexCell cell)
    {

        //SetLocation(cell);
        WaitForSeconds wait = new WaitForSeconds(oneStepTime);

        float x0 = currentCell.transform.position.x;
        float y0 = currentCell.transform.position.y + 5f + currentCell.height.y;
        float z0 = currentCell.transform.position.z;

        float x1 = cell.transform.position.x;
        float y1 = cell.transform.position.y + 5f + cell.height.y;
        float z1 = cell.transform.position.z;

        float x = x0;
        float y = y0;
        float z = z0;


        float k = -Mathf.Log(Mathf.Abs(y0 - y1) + 3f) * 200f;
        float speedX = (x1 - x0) / stepTime;
        float speedZ = (z1 - z0) / stepTime;
        float a = (y0 - y1 + k * stepTime * stepTime) / (2f * k * stepTime);
        float b = y0 - k * a * a;

        float t = 0;

        while (t < stepTime - Time.deltaTime)
        {
            t += Time.deltaTime;
            x += Time.deltaTime * speedX;
            y = k * (t - a) * (t - a) + b;
            z += Time.deltaTime * speedZ;
            transform.position = new Vector3(x,y,z);
            yield return wait;
        }
        moving = false;
        SetLocation(cell);
    }
}