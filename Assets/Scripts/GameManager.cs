﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public HexGrid fieldPrefab;
    private HexGrid fieldInstance;
    public Player playerPrefab;
    private Player playerInstance;
    public static bool playerAlive;
    public MeteorSpawner meteorSpawnerPrefab;
    private MeteorSpawner meteorSpawner;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || !playerAlive)
        {
            RestartGame();
        }
    }

    private void Start()
    {
        BeginGame();
    }

    private void BeginGame()
    {
        Camera.main.clearFlags = CameraClearFlags.Skybox;
        Camera.main.rect = new Rect(0f, 0f, 1f, 1f);
        fieldInstance = Instantiate(fieldPrefab) as HexGrid;
        fieldInstance.Generate();
        playerInstance = Instantiate(playerPrefab) as Player;
        playerInstance.Initialize(fieldInstance.cells[fieldInstance.width*fieldInstance.height / 2]);
        playerAlive = true;
        meteorSpawner = Instantiate(meteorSpawnerPrefab) as MeteorSpawner;
        meteorSpawner.Initialize(fieldInstance);
        Camera.main.clearFlags = CameraClearFlags.Depth;
        Camera.main.rect = new Rect(0f, 0f, 0.5f, 0.5f);
    }

    private void RestartGame()
    {
        StopAllCoroutines();
        Destroy(fieldInstance.gameObject);
        Destroy(meteorSpawner.gameObject);
        if (playerInstance != null)
        {
            Destroy(playerInstance.gameObject);
        }
        BeginGame();
    }
    
}